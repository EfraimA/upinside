<?php

class AcessoProtegido {
    
    public $nome;//pode ser acessado de qualquer lugar
    protected $email; //não manipulavel
    
    function __construct($nome, $email) {
        $this->nome = $nome;
        $this->setEmail($email);
        
        
    }
        //validação de email
    public function setEmail($email){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)):
            die('Email Inválido!');
        else:
            $this->email = $email;
        endif;
    }
    //compartilhar com a classe filha mas sem deixar alterar
    final protected function setNome($nome) {
        $this->nome = $nome;
        
    }
    
}

// nunca fazer a classe filha dentro de outra classe
class AcessoProtegidoFilho extends AcessoProtegido{
    
    protected $cpf;
    
    public function setCPF($nome, $cpf) {
        parent::setNome($nome);
        $this->cpf = $cpf;
        
    }
    
}