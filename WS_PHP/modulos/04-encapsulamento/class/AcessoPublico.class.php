<?php

class AcessoPublico {
    
    public $nome;
    public $email;
    
    function __construct($nome, $email) {
        $this->nome = $nome;
        $this->setEmail($email);
        
        
    }
        //validação de email
    public function setEmail($email){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)):
            die('Email Inválido!');
        else:
            $this->email = $email;
        endif;
    }
    
    
}
