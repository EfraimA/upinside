<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // Fazer consultas no banco de dados como objetos
        require ('./inc/Config.inc.php');
        $readA = new ReplicaClonagem("posts", "categoria = 'noticias'", 'ORDER BY data DESC');
        $readA->Ler();   // ler o banco de dados
        
        $readA->setTermos("categoria = 'internet'");
        
        
        $readB = clone ($readA); // ler outra parte do banco de dados
        $readB->setTermos("categoria = 'redes sociais'");// procurando a tabela 
        
        $readC = clone ($readA); // quando utiliza de clone ele não reescreve a ultama regra criada.
        $readC->setTermos("post=25");
        $readC->setTabela('comentarios');
        
        
        $readA->Ler();
        $readB->Ler();
        $readC->Ler();
        
        
        var_dump($readA, $readB, $readC);
        ?>
    </body>
</html>
