<?php
/**
 * <b> DocumentacaoDeClasse: </b>
 * Essa classe foi criada pra mostrar a interação de objetos. Logo depois
 * replicamos ela para ver a documentação de classes com PHPDoc.
 * 
 * @copyright (c) 2016, Efraim A Morais Jr UPINSIDE
 */


class DocumentacaoDeClasse {
    
    /** @var STRING Nome da empresa */
    public $empresa;
    /**
     * Esse atributo é auto gerido pela classe e incrementa o número
     * de funcionarios.
     * @var int Setores 
     */
    public $setores;
    
    /** @var InteracaoClasse    Objeto vindo de outra classe chamada
     * InteracaoClasse.     
     */    // mostrando que o atributo abaixo é de outra classe
    public $funcionario;
    
    //Constroi a classe requisitando o nome da empresa.
    function __construct($empresa) {
        $this->empresa = $empresa;
        $this->setores = 0;
    }
    
     /**
     * <b> Muda Funcionário a Editar :</b>
     * Ao executar esse método colocando um novo objeto $funcionario
     * ele vai ser editado pelos outros métodos.
     * @param object $funcionario = Objeto funcionario.
     */
    public function funcionarios($funcionario) {
        $this->funcionario = (object) $funcionario;
    }   

    /**
     * <b> Contratar Funcionário :</b> Informe um objeto da classe InteracaoClasse
     * que seria o funcionario, o carto e o salário do mesmo.
     * @param object $funcionario = Objeto da classe InteracaoClasse
     * @param string $cargo = Profissão ou cargo a ocupar
     * @param type $salario = Salários do funcionario
     */
    public function contratar($funcionario, $cargo, $salario) {
        $this->funcionario = (object) $funcionario; //interação com objetos de outra classe
        $this->funcionario->trabalhar($this->empresa, $salario, $cargo);
        $this->setores += 1;
        
    }
    
    /**
     * <b> Paga o Salário do Funcionário :</b> Ao executar esse método
     * o salário do funcionário será pago. Você ainda podera dar um echo neste
     * mesmo para visualizar o salário.
     * @return float Retorna o salário do contratado.
     */
    public function Pagar(){    
        $this->funcionario->receber($this->funcionario->salario);
        return $this->funcionario->salario;
    }
    
    /**
     * <b> Promove o Funcionário :</b> Ao executar esse método o funcionário
     * em questão é promovido ao $cargo e recebera o seguinte $salario.
     * @param string $cargo = Cargo atribuido ao funcionário.
     * @param float $salario = Salário do funcionário.
     */
    
    public function Promover($cargo, $salario = null) {
        $this->funcionario->profissao = $cargo;
        if($salario):
            $this->funcionario->salario = $salario;
        endif;
    }
    
    /**
     * <b> Demite Funcionário :</b>
     * Ao executar esse método um funcionário é demitido da empresa,
     * e recebe o dinheiro da $recisao.
     * @param float $recisao = Dinheiro a receber.
     */
    public function Demitir($recisao) {
        $this->funcionario->receber($recisao);
        $this->funcionario->empresa = Null;
        $this->funcionario->salario = Null;
        $this->funcionario->profissao = 'Mendigo';
        $this->setores -= 1;
        
    }
}
