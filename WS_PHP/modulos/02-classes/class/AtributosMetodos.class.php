<?php

class AtributosMetodos {

    //atributos
    var $Nome;
    var $Idade;
    var $Profissao;

    //metodos
    function setUsuario($Nome, $Idade, $Profissao) { //parametros
        $this->Nome = $Nome; //$this referencia os atributos diretamente
        $this->setIdade($Idade);
        $this->Profissao = $Profissao;
    }

    function getUsuario() {
        return "{$this->Nome} tem {$this->Idade} anos de idade. E trabalha como {$this->Profissao}";
    }

    function getClasse() {
        echo "<pre>";
        print_r($this);
        echo "</pre>";
    }

    function setIdade($Idade) { //Validação de idade
        if (!is_int($Idade)): //Se não for um numero inteiro da erro
            die('Idade informada é incorreta');
        else:
            $this->Idade = $Idade;
        endif;
    }
    
    function envelhecer() {
        $this->Idade = $this->Idade + 1;
    }

}
