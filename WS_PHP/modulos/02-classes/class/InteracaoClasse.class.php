<?php


class InteracaoClasse {
   
    public $nome;
    public $idade;
    public $profissao;
    public $conta;
    public $empresa;
    public $salario;
    
    
    function __construct($nome, $idade, $profissao, $conta){ // criando o metodo magic Alt+Insert
        $this->nome = $nome;
        $this->idade = $idade;
        $this->profissao = $profissao;
        $this->conta = $conta;
    }
    
    public function trabalhar($empresa, $salario, $profissao){
        $this->empresa = $empresa;
        $this->salario = $salario;
        $this->profissao = $profissao;
    }
    
    public function receber($valor){
        $this->conta += $valor;   
    }
        
    }
    