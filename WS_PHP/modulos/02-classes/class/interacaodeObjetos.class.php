<?php

class interacaodeObjetos {
    public $empresa;
    public $setores;
    
    /** @var InteracaoClasse */    // mostrando que o atributo abaixo é de outra classe
    public $funcionario;
    
    function __construct($empresa) {
        $this->empresa = $empresa;
        $this->setores = 0;
    }

    public function contratar($funcionario, $cargo, $salario) {
        $this->funcionario = (object) $funcionario; //interação com objetos de outra classe
        $this->funcionario->trabalhar($this->empresa, $salario, $cargo);
        $this->setores += 1;
        
    }
    
    public function Pagar(){    
        $this->funcionario->receber($this->funcionario->salario);
    }
    
    public function Promover($cargo, $salario = null) {
        $this->funcionario->profissao = $cargo;
        if($salario):
            $this->funcionario->salario = $salario;
        endif;
    }
    
    public function funcionarios($funcionario) {
        $this->funcionario = (object) $funcionario;
    }
    
    public function Demitir($recisao) {
        $this->funcionario->receber($recisao);
        $this->funcionario->empresa = Null;
        $this->funcionario->salario = Null;
        $this->funcionario->profissao = 'Mendigo';
        $this->setores -= 1;
        
    }
}
