<?php


class ReplicaClonagem {
    var $tabela;
    var $termos;
    var $addQuery;
    var $query;
    
    function __construct($tabela, $termos, $addQuery) {
        $this->tabela = $tabela;
        $this->termos = $termos;
        $this->addQuery = $addQuery;
    }

    function setTabela($tabela) {
        $this->tabela = $tabela;
        
    }
    
    function setTermos($termos) {
        $this->termos = $termos;
    }       
    
    function Ler(){
        $this->query = "SELECT * FROM {$this->tabela} WHERE {$this->termos} {$this->addQuery}";
        echo "{$this->query}<HR>";
    }
    
    
    
 }
