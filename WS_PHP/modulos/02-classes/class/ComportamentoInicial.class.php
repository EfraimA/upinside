<?php

class ComportamentoInicial {
    
    var $nome,$idade, $profissao, $salario;
    
    function __construct($nome,$idade, $profissao, $salario) {
        $this->nome = (string) $nome;
        $this->idade = (int) $idade;
        $this->profissao = (string) $profissao;
        $this->salario = (float) $salario;
        echo "O Objeto {$this->nome} foi iniciado<br>";
        
    }
    
    function __destruct() {
         echo "O Objeto {$this->nome} foi destruido<br>";
    }
    
    function ver (){
        echo "<pre>";
        print_r($this);
        echo "</pre>";
        
    }
}
