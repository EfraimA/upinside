<?php


class modelagemDeClasses {
   
    public $nome;
    public $idade;
    public $profissao;
    public $contaSalario;
    
    function __construct($nome, $idade, $profissao, $contaSalario){ // criando o metodo magic Alt+Insert
        $this->nome = $nome;
        $this->idade = $idade;
        $this->profissao = $profissao;
        $this->contaSalario = $contaSalario;
    }
    
    public function trabalhar($trabalho, $valor){  //criando automaticamente função publica ifnc 
        $this->contaSalario += $valor;
        $this->DarEcho("{$this->nome} Desenvolveu um {$trabalho} e recebeu {$valor}");
    }
    function setNome($nome) {
        $this->nome = $nome;
    }

    function setIdade($idade) {
        $this->idade = $idade;
    }

    function setProfissao($profissao) {
        $this->profissao = $profissao;
    }

    function setContaSalario($contaSalario) {
        $this->contaSalario = $contaSalario;
    }

    public function toReal($valor){ // formatar os numeros inteiros para real
       return number_format($valor, '2', '.', ','); 
    }
    
    public function DarEcho($Mensagem){
       echo "<p>{$Mensagem}</p>";
    }

    
}
