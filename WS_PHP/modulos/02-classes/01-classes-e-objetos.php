<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //chamando a classe
        require('./class/ClassesObjetos.class.php');
        
        //utilizando objeto
        $teste = new ClassesObjetos(); //cria o objeto
        $teste->getClass('Introdução', 'mostrar uma classe');
        $teste->verClass();
        
        //Set um dado direto no atributo
        $teste->Classe = 'Classe 2';
        $teste->Funcao = 'Ver os atributos';
        $teste->verClass();
        
        
        ?>
    </body>
    
</html>