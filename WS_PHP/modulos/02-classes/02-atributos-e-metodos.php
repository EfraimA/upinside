<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require ('./class/AtributosMetodos.class.php');
        
        $pessoa = new AtributosMetodos();
        $pessoa->setUsuario('Efraim',20,'Telemarketing');
        $usuario = $pessoa->getUsuario();
        echo $usuario;
        
        echo "<HR>";
        
        $pessoa->setUsuario('Marcelo', 27, 'Webmaster');
        $pessoa->setIdade(52);
        $pessoa->envelhecer();
        
        $pessoa->getClasse();
        
        ?>
    </body>
    
</html>