<?php

header('content-type: text/html; charset=utf-8');

$ola = "Olá Mundo!";
$nome = "Efraim";

//Jogando na tela e concatenando com variaveis e strings
echo $ola . " meu nome é " . $nome;
echo "<hr>";
echo"{$ola},Meu nome é {$nome}";
echo "<hr>";

//Jogando na tela com array
$Arr = array(
  "nome" => "Efraim",
  "sobrenome" => "Andrade"
);

print_r($Arr);

echo "<hr>";
//Debug do codigo
var_dump($Arr);
echo "<hr>";
?>


<article>
    <h1><?= $ola; ?></h1>
    <h3>Meu nome é <?= $nome; ?></h3>
    
</article>