<?php
//ALTERANDO DOC PARA HTML//
header('content-type: text/html; charset=utf-8');

//Variaveis normais
$meuNome = "Efraim A Morais Jr";
$meuTrab = "Datametrica";

$idadeDoCliente = 20;

echo "Meu nome é {$meuNome}, eu trabalho na {$meuTrab} e tenho {$idadeDoCliente} anos!<hr>";

// Referenciando com variaveis

$var = "empresa";
$$var= "Datametrica";

echo "A {$var} que eu trabalho é a {$empresa}. <hr>";

//Concatenação de Variavel

$Nome = "Efraim";
$Nome .= " A. Morais Jr";

echo " {$Nome} <hr>";

//Pegando valor de outra variavel
$Nome = $meuNome;

echo "{$Nome} <hr>";