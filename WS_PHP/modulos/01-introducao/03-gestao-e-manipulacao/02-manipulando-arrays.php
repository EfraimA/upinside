<?php

//ALTERANDO DOC PARA HTML//
header('content-type: text/html; charset=utf-8');

// Fazendo uma array de 2 jeitos, com indice automatico
$Arr =  array('PHP', 'HTML', 'CSS');
$Arr [] = 'JS';

var_dump($Arr);

$ArrB = ['PHP', 'HTML', 'CSS'];
$ArrB[] = 'JS';

var_dump($ArrB);

foreach ($ArrB as $mostraArr):
    echo "{$mostraArr} <br>";
endforeach;
echo "<HR>";

// ARRAY ASSOCIOATIVO
$ArrC = [
    "cargo" => "Programador",
    "salario" => 2200
];
$ArrC['empresa'] = "UpInside <?"; // com erros sendo tratado nas linhas abaixo
$ArrC['cargo'] = "WebMaster";
$ArrC['salario'] += 5000;
$ArrC['cargo'] .= "/Programador";

//Corrigindo pequenos error dentro do array
$ArrC = array_map('strip_tags', $ArrC); // tira inicializadores de código como <?
$ArrC = array_map('trim', $ArrC); // tira espaçamentos desnecessários
var_dump($ArrC);

//Jeito cru
echo "Eu sou {$ArrC['cargo']} na {$ArrC['empresa']}!";
echo "<HR>";
//Transformando em variaveis
extract($ArrC);
echo "Eu sou {$cargo} na {$empresa}, e ganho R$". number_format($salario, 2, ',', '.') . " por mês!";
echo "<HR>";


//Array Multidimensional, array dentro de outro array
$colaboradores = [];
$colaboradores[] = ["nome" => "Efraim Andrade", "salario" => 1144, "cargo" => "OP.Telemarketing"];
$colaboradores[] = ["nome" => "Raquel Viana", "salario" => 1000, "cargo" => "OP.Telemarketing"];

foreach ($colaboradores as $mostraColaboradores):
    extract($mostraColaboradores);
    echo "{$nome} e {$cargo} e recebe " . number_format($salario, 2, ',', '.') . " por mês!<br>"; 
endforeach;

var_dump($colaboradores);
echo "<hr>";


echo str_repeat("<br>", 20);