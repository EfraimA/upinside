<?php

//ALTERANDO DOC PARA HTML//
header('content-type: text/html; charset=utf-8');

//WHILE: enquanto acontecer
$i = 1;

while ($i < 10):
    echo "{$i} é menor que 10 mesmo <br>";
    $i++;
    if ($i >= 10):
        echo "É amigos chegamos ao {$i}";
        break;
    endif;
endwhile;
echo "<hr>";
// TABUADA COM WHILE
$z = 1;

while ($z <=10):
    echo "{$z} x 8 = " . $z * 8 . "<br>";
    $z++;
    
endwhile;
echo "<hr>";

//FOR = execute x Vezes
for ($x = 1; $x <= 10; $x++):
    echo "{$x} x 7 = ". $x * 7 . "<br>";
endfor;
echo "<hr>";

//FOREACH: arrays
$arr = ['WS php', 'WS HTML5', 'WS RWD', 'WS PP'];
foreach ($arr as $cursos):
    echo "O treinamento tem os cursos {$cursos}<BR>";
endforeach;

$arrName = ['name' => 'Efraim', "idade" => 28];
foreach ($arrName as $nome => $idade):
    echo "{$nome} = {$idade}<BR>";
endforeach;
 