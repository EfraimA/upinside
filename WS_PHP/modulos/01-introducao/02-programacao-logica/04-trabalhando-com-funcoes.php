<?php

//ALTERANDO DOC PARA HTML//
header('content-type: text/html; charset=utf-8');

//FUNÇÕES AUTOMATIZADAS
function Tabuada (){
    echo "<b> Tabuada do 5: </b><BR>";
    for($x=1; $x<=10; $x++):
        echo "5 x {$x} = ". 5 * $x . "<BR>";
    endfor;
    echo "<hr>";
}

echo Tabuada();

function myTabuada ($numero){
    echo "<b> Tabuada do $numero: </b><BR>";
    for($x=1; $x<=10; $x++):
        echo "{$numero} x {$x} = ". $numero * $x . "<BR>";
    endfor;
    echo "<hr>";
}

echo myTabuada(7);
