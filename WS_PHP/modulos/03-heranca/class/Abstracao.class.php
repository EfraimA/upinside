<?php

/**
 * Quando a classe é colocada com o parametro abstract ela não pode
 * ser utilizada, ela serve apenas como uma classe base para outras
 * classes ou seja herança. "SUPER CLASSE".
 * 
 * Se for colocado final ao inves de abstract a classe não pode ser
 * herdada ou alterada mas pode ser utilizada.
 *  */

abstract class Abstracao {
    
    public $Cliente;
    public $Conta;
    public $Saldo;
    
    function __construct($Cliente, $Saldo) {
        $this->Cliente = $Cliente;
        $this->Saldo = $Saldo;
    }
    
    public function Depositar($Valor) {
        $this->Saldo += (float) $Valor;
        echo "<span style='color: green'><B> {$this->Conta} </B>Depósito de {$this->Real($Valor)} efetuado com sucesso</span><br>";
    }
    
    public function Sacar($Valor) {
        $this->Saldo -= (float) $Valor;
        echo "<span style='color: red'><B> {$this->Conta} </B>Saque de {$this->Real($Valor)} efetuado com sucesso</span><br>";        
    }
    
    /** 
     * 
     * @param Abstracao $Destino
     */
    public function Transferir($Valor, $Destino) {
        if($this === $Destino):
            echo "Você não pode tranferir valores para a mesma conta <br>";
        else:
            echo "<hr>";
            $this->Sacar($Valor);
            $Destino->Depositar($Valor);
            echo "<span style='color: blue'><B> {$this->Conta} </B>Transferencia de {$this->Real($Valor)} efetuada com sucesso de {$this->Cliente} para {$Destino->Cliente}</span><br>";
            echo "<hr>";
        endif;
        
    }
    
    public function Extrato() {
        echo "<hr><hr> Olá {$this->Cliente}. Seu saldo em {$thi->Conta} é de {$this->Real($this->Saldo)} <hr>";
    }
    
    public function Real($Valor) {
        return "R$" . number_format($Valor, '2', '.', ',');
        
    }
    
}
