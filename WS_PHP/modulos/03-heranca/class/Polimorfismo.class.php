<?php

/**
 * <b> Classe tipos de pagamentos raiz :</b>
 * Será usado como base para os tipos de pagamentos como depósito, 
 * cartão e boleto
 * Focado em pagamentos via boleto
 */
class Polimorfismo {
    
    public $produto;
    public $valor;
    public $metodo;
    
    /**
     * <b> Entrada de Produto e Valor :</b>
     * Ao criar o objeto é necessário um $produto e o $valor do mesmo.
     * @param string $produto
     * @param float $valor
     */
    function __construct($produto, $valor) {
        $this->produto = $produto;
        $this->valor = $valor;
        $this->metodo = 'Boleto';
    }

    /**
     * <b> Mostra o Valor e tipo de pagamento :</b>
     * Com duas saidas informa o valor, produto e tipo de pagamento utilizado.
     */
    public function Pagar() {
        echo "Você pagou {$this->Real($this->valor)} por um {$this->produto} <br>";
        echo "<small> Pagamento efetuado via {$this->metodo}</small><br>";
    }
    
    /**
     * <b> Executa uma formatação para real :</b>
     * Ao executar esse método o numero float de entrada é formatado para seu
     * valor em R$.
     * @param float $valor
     * @return formact
     */
    public function Real($valor) {
        return "R$ " . number_format($valor, '2', '.', ',');
    }
}
