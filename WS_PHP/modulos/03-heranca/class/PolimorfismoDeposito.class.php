<?php

/**
 * <b> Classe de Deposito : </b>
 * Executa as funções referentes a depósito.
 */
class PolimorfismoDeposito extends Polimorfismo{
    
    public $desconto;
    
    /**
     * <b> Adiciona o metodo Desconto :</b>
     * Quando é criado um objeto novo por essa classe de herança existe um 
     * $desconto por conta do metodo de pagamento
     * @param int $desconto = Desconto no preço final do produto
     */
    function __construct($produto, $valor) {
        parent::__construct($produto, $valor);
        $this->desconto = 15;  //porcentagem de desconto
        $this->metodo = 'Depósito';
    }
    
    /**
     * <b> Metodo de mostrar desconto :</b>
     * Mostra quanto é o desconto.
     * @param int $desconto = Desconto no produto final.
     */
    function setDesconto($desconto) {
        $this->desconto = $desconto;
    }
    
    /**
     * <b> Função pagar :</b>
     * Executa o $desconto no valor final do produto.
     */
    public function Pagar() {
        $this->valor = ($this->valor / 100) * (100 - $this->desconto);
        parent::Pagar();
    }
}
