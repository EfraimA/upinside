<?php



class HerancaJuridica extends Heranca{//extends indica a classe de herança
    
    public $empresa;//novos atributos para a herança
    public $funcionarios;
    
    function __construct($nome, $idade, $empresa) {
        parent::__construct($nome, $idade);//pega os atributos da classe herança
        $this->empresa = $empresa;//adiciona um novo atributo 
    }
    
    public function Contratar($pessoa){
        echo "A empresa {$this->empresa} de {$this->nome} contratou {$pessoa} . <hr>";
        $this->funcionarios += 1; 
    }
    
    public function VerEmpresa (){
        echo "{$this->empresa} foi fundada por {$this->nome} e tem {$this->funcionarios} funcionarios<br><small style='color:#09f;'>";
        parent::VerPessoa(); // usaa function da classe de herança
        echo "</small>";
    }
    
}
