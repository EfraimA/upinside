<?php

/**
 * <b> Classe de Cartão de Crerdito : </b>
 * Executa as funções referentes a Cartão de Crerdito.
 */

class PolimorfismoCartao extends Polimorfismo{
    
    public $juros;
    public $encargos;
    public $parcela;
    public $numParcelas;
    
        /**
     * <b> Adiciona o metodo Juros :</b>
     * Quando é criado um objeto novo por essa classe de herança existe um 
     * $juros por conta do metodo de pagamento
     * @param int $juros = Desconta no preço final do produto
     */
    function __construct($produto, $valor) {
        parent::__construct($produto, $valor);
        $this->juros = 1.17;
        $this->metodo = 'Cartão de Credito';
    }
    
    /**
     * <b> Função Pagar :</b>
     * Faz os descontos de juros no valor final e mostra em tela.
     * @param int $parcelas = Numero de vezes que será feito o produto.
     */
    public function Pagar($parcelas = null) {
        $this->setNumParcelas($parcelas);
        $this->setEncargos();
        
        $this->valor = $this->valor + $this->encargos;
        $this->parcela = $this->valor / $this->numParcelas;
        
        echo "Você pagou {$this->Real($this->valor)} por um {$this->produto}<bR>";
        echo "<small>Pagamento efetuado via {$this->metodo} em {$this->numParcelas}x iguais de {$this->Real($this->parcela)}</small><br>";
    }
    
    /**
     * Para 5,5% informe 5.5
     */
    function setJuros($juros) {
        $this->juros = $juros;
    }
    
    /**
     * <b> Faz a conta de encargos :</b>
     * Executa a função jogando no valor final do produto junto com os juros.
     */
    function setEncargos() {
        $this->encargos = ($this->valor * ($this->juros /100)) * $this->numParcelas;
    }

    /**
     * <b> Informa se vai ser parcelado ou não :</b>
     * Ao executar informa se vai ser parcelado e em quantas parcelas será.
     * @param int $numParcelas = Numero de parcelas.
     */
    function setNumParcelas($numParcelas) {
        $this->numParcelas = ( (int) $numParcelas >= 1 ? $numParcelas : 1 );
    }


}
