<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require ('./inc/Config.inc.php');
        
        $pessoa = new Heranca('Efraim A. M. Junior', 20);
        $pessoa->Formar('WS PHP');
        $pessoa->Formar('pro PHP');
        $pessoa->Envelhecer();
        $pessoa->VerPessoa();
        
        
        var_dump($pessoa);
        echo "<HR>";
        
        $pessoaJUR = new HerancaJuridica('Efraim A. M. Junior', 20, 'UPINSIDE');
        $pessoaJUR->Formar('WS PHP');
        $pessoaJUR->Formar('pro PHP');
        $pessoaJUR->Envelhecer();
        $pessoaJUR->VerPessoa();
       
        
        $pessoaJUR->Contratar('Raquel V. Assumpção');
        $pessoaJUR->Contratar('Valdineia S.');
         $pessoaJUR->VerEmpresa();
        
        var_dump($pessoaJUR);
        
        ?>
    </body>
    
</html>