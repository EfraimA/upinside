<?php

define('HOME', 'http://127.0.0.1/PHP-upinside/HTML5/Modulos/06-micro-formatos');
define('THEME', 'wshtml');

define('INCLUDE_PATH', HOME . '/themes/' . THEME);
define('REQUIRE_PATH', 'themes/' . THEME);


$getUrl = strip_tags(trim(filter_input(INPUT_GET, 'url', FILTER_DEFAULT)));
$setUrl = (empty($getUrl) ? 'index' : $getUrl);
$Url = explode('/', $setUrl);

$pg_name = 'Taberna Yugioh!';
$pg_site = 'Taberna Yugioh!';
$pg_google_author = '116776011488186780984';
$pg_google_publisher = '116776011488186780984';
$pg_fb_app = '202941230153632';
$pg_fb_author = 'efraim.andrade.3';
$pg_fb_page = 'avacynstore';
$pg_twitter = '@eflolz';
$pg_domain = 'www.tabernayugioh.com.br';
$pg_sitekit = INCLUDE_PATH . 'img/sitekit/';

switch ($Url[0]):
    case 'index':
        $pg_title = $pg_name;
        $pg_desc = 'O Lugar Onde Você Vai Desfrutar do Mundo de Yugioh!';
        $pg_image = $pg_sitekit . 'index.png';
        $pg_url = HOME;
        break;
    case 'sobre':
        $pg_title = 'Informações Adicionais Referente ao Taberna Yugioh';
        $pg_desc = 'Veja mais detalhes referente ao nosso site focado em <b>Yugioh!</b>';
        $pg_image = $pg_sitekit . 'sobre.png';
        $pg_url = HOME . '/sobre';
        break;
    default:
        $pg_title = '404 Não Encontrado';
        $pg_desc = 'Você está vendo agora a página 404 pois não foi encontrado conteúdo relacionado a <b>' . $setUrl . '</b>, mas não saia ainda. Temos algumas dicas para te ajudar com sua pesquisa!';
        $pg_image = $pg_sitekit . 'index.png';
        $pg_url = HOME . '/404';
        break;
endswitch;




