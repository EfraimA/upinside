<article class="container">
    <div class="content" style="padding-bottom: 10px;">
        <header class="sectiontitle">
            <h1><?= $pg_title; ?></h1>
            <p class="tagline"><?= $pg_desc; ?></p>
        </header>

        <div class="trigger trigger-infor al-center">Está página está interiamente servida com Micro Formatos. Isso é lindo! Isso é Otimização Pura!</div>
        <div class="clear"></div>
    </div>
</article>

<section class="container bg-light main_bus" itemscope itemtype="https://schema.org/Organization">
    <div class="content">

        <header class="articletitle fl-left" style="width: 70%;">
            <h1>Conheça a <span itemprop="name">UpInside Treinamentos</span></h1>
            <p class="tagline">Saiba mais sobre a escola que ensina o método HTML5 do Jeito Certo!</p>
        </header>

        <img itemprop="image" class="fl-right boxshadow" style="width: 27%;" src="<?= HOME; ?>/uploads/mod6/upinside-treinamentos.jpg" alt="[UpInside Treinamentos]" title="UpInside Treinamentos - Cursos Profisionais em TI!"/>

        <article class="m-bottom">
            <div class="box-large">
                <h1 class="fontzero">Sobre a UpInside:</h1>
                <p itemprop="description">Fundada em 2010, a UPINSIDE se destaca por ter os melhores e mais completos cursos de desenvolvimento. Cursos profissionais, 100% em vídeo aulas, com suporte e certificação!</p>
                <p>Mas não ficamos por ai, além disso a UpInside conta com a mais avançada e intuitiva plataforma do mercado, com um ambiente responsivo onde o aluno conta com um suporte diferencial. Podendo acessar seus cursos quando e onde quiser.</p>
            </div>
        </article>

        <article class="box box-medium">
            <h1 class="title">Canais de Atendimento:</h1>
            <ul>
                <li><b>Por Telefone:</b> <span itemprop="telephone">54 3381.218</span></li>
                <li><b>Por E-mail:</b><span itemprop="email">cursos@upinside.com.br</span></li>
                <li><b>Site Oficial:</b> <a itemprop="sameAs" href="https://www.upinside.com.br" title="UpInside Treinamentos">upinside.com.br</a></li>
            </ul>

            <p class="address" itemprop="address" itemscope itemtype="https://schema.org/Address">
                <span itemprop="streetAddress">Rua Lourival da Cruz 174</span>,<br>
                <span itemprop="postalCode">99300000</span> / <span itemprop="addressLocatily">Soledade</span>, <span itemprop="addressRegion">RS</span><br>
                <span itemprop="addressCountry">Brasil</span
                <span itemprop="geoCoordinates" itemscope itemtype="https://schema.org/GeoCoordinates">
                    <meta itemprop="latitude" content="-28.831887" />
                    <meta itemprop="longitude" content="-52.501189" />
                </span>
            </p>
        </article>

        <aside>
            <h1 class="title">Redes Sociais:</h1>
            <ul>
                <li><a itemprop="url" class="shorticon shorticon-google" title="UpInside no Google+" href="https://plus.google.com/+upinside" target="_blank" rel="nofollow">Acompanhe no Google+</a></li>
                <li><a itemprop="url" class="shorticon shorticon-facebook" title="UpInside no Facebook" href="https://www.facebook.com/upinside" target="_blank" rel="nofollow">Curta no Facebook</a></li>
                <li><a itemprop="url" class="shorticon shorticon-twitter" title="UpInside no Twitter" href="https://twitter.com/UpInsideBr" target="_blank" rel="nofollow">Siga no Twitter</a></li>

            </ul>
        </aside>

        <div class="clear"></div>
    </div>
</section>

<article class="container bg-blue" itemscope itemtype="https://schema.org/Person">
    <div class="content">

        <img class="round fl-left boxshadow" style="width: 16%" src="<?= HOME; ?>/uploads/mod6/efraim.jpg" alt="[Robson V. Leite, Tutor do Curso WS HTML5]" title="Robson V. Leite"/>

        <div class="fl-right" style="width: 80%;">
            <header class="articletitle">
                <h1>Conheça <span itemprop="name"> Efraim A. Morais Jr.</span>. Seu tutor neste curso!</h1>
                <p class="tagline"><span itemprop="description"> Apaixonado pelo mundo web. Webmaster, trabalha com desenvolvimento,</span> SEO e divulgação desde 2006!</p>
            </header>

            <div>
                <p>Robson V. Leite fundou sua primeira agência web em 2006, Sempre em busca de conhecimento e aprimoramento em 2010 fundou o Blog UpInside, que mais tarde veio a se tornar um portal de vídeo aulas.</p>
                <p>Em 2011 iniciou a <span itemprop="affiliate">UpInside Treinamentos</span>, e como <span itemprop="jobtitle">CEO e Professor</span> vem criando os melhores e mais completos cursos em seus segmentos segundo seus alunos.</p>
                <p>Eterno aluno, apaixonado por PHP, artista da web. Esse será seu tutor neste curso.</p>
                <ul class="main_bus_person">
                    <li><a class="boxshadow" target="_blank" href="https://www.facebook.com/robson.v.leite" title="Robson V. Leite No Facebook!">No Facebook</a></li>
                    <li><a class="boxshadow g" target="_blank" href="https://plus.google.com/+RobsonVLeite/posts" title="Robson V. Leite No Google+!">No Google+</a></li>
                    <li><a class="boxshadow m" href="mailto:Robson V. Leite <cursos@upinside.com.br>" title="Enviar E-mail Para Robson V. Leite">Por E-mail</a></li>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</article>

<section class="container">
    <div class="content">
        <header class="sectiontitle sectiontitle-nomargin">
            <h1>Veja o que falam sobre o curso:</h1>
            <p class="tagline">Leia os depoimentos de quem já estuda o WS HTML5!</p>
        </header>
        <div class="clear"></div>
    </div>

    <div class="bg-bluelight">
        <div class="content content-page" style="padding-bottom: 20px;">

            <div class="m-bottom">
                <p class="fontsize2 font-light">Curso Work Series - HTML5 do Jeito Certo</p>
                <p>HTML5 do Jeito Certo trata de um conjunto de técnicas profissionais desenvolvidas e testadas pela UpInside. Fragmentando o desenvolvimento em 3 pilares para entregar um site extremamente otimizado...</p>
                <p class="fontsize2">&starf; &starf; &starf; &starf; &star;</p>
            </div>

            <?php for ($i = 0; $i < 3; $i++): ?>

                <article class="depoiment box box-large bg-light" style="padding: 20px; width: 48%; margin: 0 4% 4% 0">
                    <header class="articletitle" style=" margin-bottom: 15px; padding-bottom: 15px;  border-bottom: 1px solid #999;">
                        <img class="round fl-left boxshadow" style="width: 18%" src="<?= HOME; ?>/uploads/mod6/efraim.jpg" alt="[Efraim A. Morais Jr.]" title="Depoimento de Efraim A. Morais Jr.">
                        <div class="fl-right" style="width: 75%;">
                            <h1 style="font-size: 1.4em;">Depoimento de Robson V. Leite:</h1>
                            <p style="margin: 5px 0;">Em <time datetime="2014-09-13">9 de setembro de 2014</time></p>
                            <span>&starf; &starf; &starf; &starf; &starf;</span>
                        </div>
                    </header>

                    <p>Perfeito!</p>
                    <p>O curso é perfeito em todos os sentidos, mostrando o conteúdo completo e do zero a otimização e lançamento!</p>
                    <p>Minha nota é 5</p>
                </article>

                <article class="depoiment box box-large last bg-light" style="padding: 20px; width: 48%; margin: 0 0 4% 0;">
                    <header class="articletitle">
                        <h1>Depoimento de Graziele Valer:</h1>
                        <p style="margin: 5px 0;">Em <time datetime="2014-09-13">12 de setembro de 2014</time></p>
                        <span>&starf; &starf; &starf; &starf; &starf; &starf; &starf; &starf; &starf;  &star;</span>
                    </header>

                    <p>Quase Perfeito!</p>
                    <p>O curso é quase perfeito em todos os sentidos, Contudo é um pouco avançado, e tive um pouco de dificuldade com o CSS básico!</p>
                    <p>Minha nota foi 9 em uma escala de 0 à 10!</p>
                </article>

                <div class="box-line"></div>

            <?php endfor; ?>
            <div class="clear"></div>
        </div>
    </div>
</section>

<div class="container bg-orange">
    <div class="content content-page">
        <p class="fontsize1b font-light m-bottom al-center">Deixe seu nome e e-mail, com seu interesse e podemos te enviar uma oferta!</p>
        <form name="sendcontent" action="obrigado" method="post">
            <input class="box box-medium" type="text" title="Informe Seu Nome" name="nome" placeholder="Informe Seu Nome:"/>
            <input class="box box-medium" type="email" title="Informe Seu E-mail" name="email" placeholder="Informe Seu E-mail:"/>

            <a href="#" class="btn btn-green radius box box-medium last al-center">Quero Fazer Esse Curos!</a>
        </form>
        <div class="clear"></div>
    </div>
</div>

<section class="container bg-gray">
    <div class="content">
        <header class="sectiontitle">
            <h1>Confira as Próximas Turmas:</h1>
            <p class="tagline">Veja abaixo a lista de datas e turmas para o WS HTML5!</p>
        </header>

        <?php
        for ($i = 1; $i <= 3; $i++):
            $last = ($i % 3 == 0 ? ' last' : '');
            $x = $i * 2;
            ?>
            <article class="box box-medium bg-blue boxshadow<?= $last; ?> " style="font-size: 0.8em; padding: 25px;">
                <header class="articletitle">
                    <h1>WS HTML5 - Turma 2</h1>
                    <p>Abertura de 50 vagas. Segunda turma do curso WS HTML5 do Jeito Certo.</p>
                </header>

                <p>
                    <time datetime="2014-10-20T08:20:00-03:00"><b>Abertura:</b> 20 de outubro de 2014 às 8:20hs</time><br>
                    <time datetime="2014-10-27T23:59:00-03:00"><b>Fechamento:</b> 27 de outubro de 2014 às 23:59hs</time>
                </p>

                <span>Turor: <span>Robson V. Leite</span></span>
                <link href="https://www.upinside.com.br"/>
                <link href="<?= HOME; ?>/uploads/mod6/robson-v-leite.jpg"/>
                <p>Tempo de curso: 30 horas</p>

                <p>
                    Centro de Eventos<br>
                    Rua 15 de Novembro 982,<br>
                    99300000 / Soledade, RS<br>
                    Brasil
                </p>

                <p>Valor: R$ 680,00 - 50 vagas</p>

                <a class="btn btn-green radius" title="Quero Participar" href="https://www.upinside.com.br/cesta/index/novo-item/curso-html5">Matrícula</a>
            </article>
        <?php endfor; ?>

        <div class="clear"></div>
    </div>
</section>