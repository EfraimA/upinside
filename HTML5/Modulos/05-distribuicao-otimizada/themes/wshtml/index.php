<!--Bloco de Video // A parte principal do site onde mostra os destaques e conteudos mais interessantes!-->
<article class="container">
    <div class="content">
        <header>
            <div class="sectiontitle">
                <h1>Conheça o Acervo das Cartas</h1>
                <p class="tagline">Contemple o site que faltava sobre<mark>Yugioh!</mark></p>
            </div>
        </header>

        <video class="video video-slarge main_video" src="<?= HOME; ?>/uploads/medias/Noragami_01_Dollars-OtakusFans-Anbient.mkv" width="400" controls></video>

        <aside class="al-center">
            <h1>Não Deixe de Visitar Nossa Loja Virtual <a class="btn_video btn btn-red radius" title="Comprar Curso de HTML5 Agora!" target="_blank" href="http://upinside.com.br">Conhecer!</a></h1>
        </aside>

        <div class="clear"></div>
    </div>

    <footer class="container bg-bluelight">
        <section class="content main_videos" style="padding-bottom: 10px;">
            <h1>Veja Alguns Setores do Site Onde Você Pode Explorar!</h1>

            <article class="box box-small">
                <div class="thumb">
                    <div class="video_play"></div>
                    <img title="Sessão de Artigos Sobre Yugioh" alt="Artigos Yugioh" src="<?= INCLUDE_PATH; ?>/img/entendendo-o-html5.jpg"/>
                </div>
                <h1 class="box_video_title">Leia Artigos e Estratégias Sobre Yugioh!</h1>
            </article> 

            <article class="box box-small">
                <div class="thumb">
                    <div class="video_play"></div>
                    <img title="Construa e Compartilhe Seu Próprio Deck de Yugioh!" alt="Criar Decks Yugioh" src="<?= INCLUDE_PATH; ?>/img/entendendo-o-html5.jpg"/>
                </div>
                <h1 class="box_video_title">Construa e Compartilhe Seu Próprio Deck! </h1>
            </article> 

            <article class="box box-small">
                <div class="thumb">
                    <div class="video_play"></div>
                    <img title="Pesquisar Decks de Yugioh" alt="Pesquisar Decks de Yugioh" src="<?= INCLUDE_PATH; ?>/img/entendendo-o-html5.jpg"/>
                </div>
                <h1 class="box_video_title">Procure Por um Deck!</h1>
            </article> 

            <article class="box box-small last">
                <div class="thumb">
                    <div class="video_play"></div>
                    <img title="Assistir Yugioh Online" alt="Assistir Yugioh Online" src="<?= INCLUDE_PATH; ?>/img/entendendo-o-html5.jpg"/>
                </div>
                <h1 class="box_video_title">Assista Yugioh Online!</h1>
            </article> 


        </section>
    </footer>
</article>

<!--Sessão Relacional-->

<section class="container bg-orange">
    <!--Container do Titulo-->
    <div class="content">
        <div class="sectiontitle sectiontitle-nomargin">
            <h1>Nesta sessão você irá encontrar todo tipo de conteúdo relacionado a Yugioh!</h1>
            <p class="tagline">Conheça as estratégias e artigos úteis para você incrementar ao seu jogo, além da ecônomia das cartas notícias e muito mais!</p>
        </div>
        <div class="clear"></div>
    </div>

    <!--Container dos Artigos-->
    <div class="container bg-body">

        <div class="content" style="padding-bottom: 10px;">

            <article class="main_tec_item box box-small al-center radius">
                <img src="<?= INCLUDE_PATH; ?>/img/tec_semantic.png" alt="HTML5 Semântico" title="Módulo Semântico"/>
                <h1>Últimas Notícias</h1>
                <p class="tagline">Notícias Sobre Yugioh!</p>
            </article>

            <article class="main_tec_item box box-small al-center radius">
                <img src="<?= INCLUDE_PATH; ?>/img/tec_drycss.png" alt="CSS produtivo com OOCSS" title="CSS produtivo OOCSS"/>
                <h1>Artigos Úteis</h1>
                <p class="tagline">Últimos Artigos Lançados no Site</p>
            </article>

            <article class="main_tec_item box box-small al-center radius">
                <img src="<?= INCLUDE_PATH; ?>/img/tec_forms.png" alt="Formulários" title="Módulo de Formulários"/>
                <h1>Top Decks</h1>
                <p class="tagline">Melhores Decks Profissionais e Amadores</p>
            </article>

            <article class="main_tec_item box box-small al-center radius last">
                <img src="<?= INCLUDE_PATH; ?>/img/tec_midia.png" alt="Àudio e Vídeo na Web" title="Módulo de Áudio e Vídeo na Web"/>
                <h1>Cartas em Alta</h1>
                <p class="tagline">Cartas Que Estão em Alta</p>
            </article>

            <div class="box-line"></div>

            <article class="main_tec_item box box-small al-center radius">
                <img src="<?= INCLUDE_PATH; ?>/img/tec_geo.png" alt="Geolocation e HTML5 Storage" title="Módulode Geolocation e HTML5 Storage"/>
                <h1>Cartas em Queda</h1>
                <p class="tagline">Cartas Que Estão em Baixa</p>
            </article>

            <article class="main_tec_item box box-small al-center radius">
                <img src="<?= INCLUDE_PATH; ?>/img/tec_microdados.png" alt="Distribuição com Micro Dados" title="Módulo de Distribuição com Micro Dados"/>
                <h1>Cartas Mais Pesquisadas</h1>
                <p class="tagline">Cartas Que Está Sendo Mais Procuradas em Nosso Acervo</p>
            </article>

            <div class="clear"></div>
        </div>
    </div>
</section>

<!--Seção Temática-->
<section class="container bg-bluelight">
    <div class="content">
        <div class="sectiontitle">
            <h1  class="shorticon shorticon-config shorticon-sectiontitle d-inline-block">Top Decks</h1>
            <p class="tagline">Saiba Tudo Sobre Os Melhores Decks Utilizados em Campeonatos!!</p>
        </div>

        <article class="main_info box box-medium2"><h1>Deck: <b>Olhos Vermelhos</b></h1></article>
        <article class="main_info box box-medium2"><h1>Tipo: <b>Dragão</b></h1></article>
        <article class="main_info box box-medium2 last"><h1>Elemento: <b>Dark</b></h1></article>
        <article class="main_info box boxfull box-line"><h1>Descrição: <b>Um deck baseado no clássico Dragão Negro de Olhos Vermelhos, voltado para um jogo focado em criaturas fortes e invocação especial do cemitério!</b></h1></article>



        <div class="clear"></div>
    </div>
</section>

<!--Retomada e Conversão-->
<article class="container bg-blue">
    <div class="content content-page al-center retomada_title">

        <header>
            <div class="sectiontitle">
                <h1>Faça Um Cadastro Em Nosso Site!</h1>
                <p class="tagline">Começe Agora Mesmo.  <mark>Crie Já Seu Deck</mark> Totalmente <mark>Grátis</mark> e, <mark>Online!</mark></p>
            </div>
        </header>

        <a class="btn btn-green btn-big radius" title="Quero me Cadastrar no Site!" target="_blank" href="http://www.upinside.com.br">Cadastre-se no Site!</a>

        <footer>
            <div class="main_chamdada al-center">
                Você fica sabendo de tudo sobre Yugioh a qualquer momento e em qualquer local em nosso site preferido da internet!
            </div>
        </footer>

        <div class="clear"></div>
    </div>
</article>


