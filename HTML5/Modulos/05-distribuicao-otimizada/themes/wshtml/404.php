<section class="container">
    <div class="content">
        <header class="sectiontitle sectiontitle-nomargin">
            <h1><?= $pg_title; ?></h1>
            <p class="tagline"><?= $pg_desc; ?></p>
        </header>
        <div class="clear"></div>
    </div>

    <article class="bg-light">
        <div class="content">
            <header class="articletitle">
                <h1>Deixe uma sugestão de conteúdo:</h1>
                <p class="tagline">Informe seu nome e e-mail para sugerir conteúdo relacionado à <?= $setUrl; ?></p>
            </header>

            <form name="sendcontent" action="<?= HOME; ?>/obrigado" method="post">
                <input class="box box-medium"  type="text" title="Informe Seu Nome" name="nome" placeholder="Informe Seu Nome: "/>
                <input class="box box-medium" type="email" title="Informe Seu E-mail" name="email" placeholder="Informe Seu E-mail: "/>

                <a href="#" class="btn btn-green radius box box-medium last al-center">Deixe Sua Sugestão</a>
            </form>
            <div class="clear"></div>
        </div>
    </article>

    <section>
        <div class="content al-center">
            <header class="articletitle">
                <h1>Veja conteúdo relacionado a sua pesquisa:</h1>
                <p class="tagline">Veja o que encontramos ao pesquisar por <b><?= $setUrl; ?></b> em nossa base de conteúdo!</p>
            </header>



            <article class="related_item box box-small">
                <div class="video_play"></div>
                <img title="Vídeo Aula Entenda o HTML5" alt="Entenda o HTML5" src="<?= HOME; ?>/uploads/images/kit.jpg"/>
                <h1 class="box_video_title"><a href="#link" title="Ver Mais Sobre Entenda HTML5">Entenda o HTML5</a></h1>
                <p class="tagline">Entendendo o html5 mesmo eoqqsdadsadsadsa</p>
            </article>

            <article class="related_item box box-small">
                <div class="video_play"></div>
                <img title="Vídeo Aula Entenda o HTML5" alt="Entenda o HTML5" src="<?= HOME; ?>/uploads/images/kit.jpg"/>
                <h1 class="box_video_title"><a href="#link" title="Ver Mais Sobre Entenda HTML5">Entenda o HTML5</a></h1>
                <p class="tagline">Entendendo o html5 mesmo eoqqsdadsadsadsa</p>
            </article>

            <article class="related_item box box-small">
                <div class="video_play"></div>
                <img title="Vídeo Aula Entenda o HTML5" alt="Entenda o HTML5" src="<?= HOME; ?>/uploads/images/kit.jpg"/>
                <h1 class="box_video_title"><a href="#link" title="Ver Mais Sobre Entenda HTML5">Entenda o HTML5</a></h1>
                <p class="tagline">Entendendo o html5 mesmo eoqqsdadsadsadsa</p>
            </article>

            <article class="related_item box box-small last">
                <div class="video_play"></div>
                <img title="Vídeo Aula Entenda o HTML5" alt="Entenda o HTML5" src="<?= HOME; ?>/uploads/images/kit.jpg"/>
                <h1 class="box_video_title"><a href="#link" title="Ver Mais Sobre Entenda HTML5">Entenda o HTML5</a></h1>
                <p class="tagline">Entendendo o html5 mesmo eoqqsdadsadsadsa</p>
            </article>

    
            <div class="clear"></div>
        </div>
    </section>

    <nav class="bg-orange">
        <div class="content">
            <h1 class="fontzero">Acesse nosso conteúdo principal:</h1>
            <ul class="reverse_nav">
                <?php require REQUIRE_PATH . '/inc/menu_nav.php'; ?>
            </ul>
            <div class="clear"></div>
        </div>
    </nav>

</section>

















