<!DOCTYPE html>
<?php
define('BASE', 'http://127.0.0.1/PHP-upinside/HTML5/Modulos/02-html5-semantico/');
?>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Curso Work Series - HTML5 do Jeito Certo</title>
        <link rel="stylesheet" href="css/oocss.css" />

        <!--[if it IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->

    </head>
    <body>
        <header class="container bg-blue">
            <div class="content">
                <h1 class="title">
                    <a title="Home" href="<?= BASE; ?>">
                        Curso Work Series - HTML5 do Jeito Certo!
                    </a>
                </h1>

                <ul>
                    <li><a title="HTML5 Semantico" href="<?= BASE; ?>/html5-semantico">Semântica</a></li>
                    <li><a title="Estilo Produtivo com OOCSS" href="<?= BASE; ?>/estilo-com-oocss">OOCSS</a></li>
                    <li><a title="Formulários com HTML5" href="<?= BASE; ?>/formularios-com-html5">Forms</a></li>
                    <li><a title="Áudio e Vídeo na Web" href="<?= BASE; ?>/audio-e-video">Mídia</a></li>
                    <li><a title="Geolocation e HTML Storage" href="<?= BASE; ?>/geolocation-e-storage">API's</a></li>
                    <li><a title="Distribuição com Micro Dados" href="<?= BASE; ?>/micro-dados">Micro Dados</a></li>
                    <li><a class="btn btn-green radius boxshadow" title="Comprar Curso" href="http://www.upinside.com.br">Comprar Curso</a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </header>

        <!--CONTEUDO-->
        <article class="container">
            <div class="content">
                <header>
                    <h1>Conheça o Curso de WS HTML5!</h1>
                    <p class="tagline">Veja oque o tutor do curso<mark>Robson V. Leite</mark> tem a dizer!</p>
                </header>

                <video src="medias/Noragami_01_Dollars-OtakusFans-Anbient.mkv" width="400" controls></video>

                <aside>
                    <h1>Pronto para <a title="Comprar Curso de HTML5 Agora!" target="_blank" href="http://upinside.com.br">Comprar Agora!</a></h1>
                </aside>

                <div class="clear"></div>
            </div>

            <footer class="bg-blue">
                <section class="content">
                    <h1>Veja Algumas Aulas do Curso WS HTML5!</h1>

                    <article>
                        <div class="thumb">
                            <img title="" alt="" src=""/>
                        </div>
                        <h1>Entenda o HTML5</h1>
                    </article> 

                    <article>
                        <div class="thumb">
                            <img title="" alt="" src=""/>
                        </div>
                        <h1>Entenda o HTML5</h1>
                    </article> 

                    <article>
                        <div class="thumb">
                            <img title="" alt="" src=""/>
                        </div>
                        <h1>Entenda o HTML5</h1>
                    </article> 

                    <article>
                        <div class="thumb">
                            <img title="" alt="" src=""/>
                        </div>
                        <h1>Entenda o HTML5</h1>
                    </article> 
                </section>
            </footer>
        </article>


        <!--CONTEUDO-->

        <footer class="container bg-light">
            <section class="main_footer content ">
                <h1 class="fontzero">Sobre  a UpInside Treinamentos</h1>

                <nav class="main_nav">
                    <h1 class="title">Mais sobre o WS HTML5:</h1>
                    <ul>
                        <li><a title="Assista o Vídeo de Apresentação" href="#apresentacao">Assista o Vídeo</a></li>
                        <li><a title="Assista o Vídeo de Apresentação" href="#apresentacao">Assista o Vídeo</a></li>
                        <li><a title="Assista o Vídeo de Apresentação" href="#apresentacao">Assista o Vídeo</a></li>
                    </ul>
                </nav>

                <article class="main_social">   
                    <h1 class="title">UpInside nas redes sociais:</h1>
                    <ul>
                        <li><a rel="nofollow" target="_blank" title="UpInside Treinamentos no Facebook" href="http://fb.com">Facebook</a></li>
                        <li><a rel="nofollow" target="_blank" title="UpInside Treinamentos no Google Plus" href="http://g.com">Google +</a></li>
                        <li><a rel="nofollow" target="_blank" title="UpInside Treinamentos no Twitter" href="http://tw.com">Twitter</a></li>
                    </ul>
                </article>

                <article class="main_copy">
                    <h1 class="fontzero">Plataforma UpInside</h1>
                    <p><b>Plataforma EAD:</b><a title="Plataforma EAD da UpInside" href="http://upinside.com.br">www.upinside.com.br</a></p>
                    <p><b>E-mail:</b>cursos@upinside.com.br</p>
                    <hr>
                    <p>&copy; <?= date('Y'); ?> - UpInside Treinamentos, Todos os Direitos Reservados!</p>
                </article>

                <div class="clear"></div>
            </section>
        </footer>
    </body>
</html>
