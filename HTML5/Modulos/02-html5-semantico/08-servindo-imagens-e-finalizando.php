<!DOCTYPE html>
<?php
define('BASE', 'http://127.0.0.1/PHP-upinside/HTML5/Modulos/02-html5-semantico/');
?>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Acervo das Cartas Yugioh!</title>

        <!--[if it IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->

        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="css/boot.css" />
        <link rel="shortcut icon" href="img/favicon.png" />

    </head>
    <body>
        <header class="container bg-blue">
            <div class="content">
                <h1 class="title">
                    <a title="Home" href="<?= BASE; ?>">
                        Acervo das Cartas!
                    </a>
                </h1>

                <ul>
                    <li><a title="Página Principal" href="<?= BASE; ?>/formularios-com-html5">Home</a></li>
                    <li><a title="Artigos de Yugioh" href="<?= BASE; ?>/html5-semantico">Artigos</a></li>
                    <li><a title="Construção de Decks" href="<?= BASE; ?>/estilo-com-oocss">Construção de Decks</a></li>
                    <li><a title="Busca de Cartas" href="<?= BASE; ?>/audio-e-video">Busca de Cartas</a></li>
                    <li><a title="Noticias Sobre Yugioh" href="<?= BASE; ?>/geolocation-e-storage">Ultimas Notícias</a></li>
                    <li><a title="Assista Yugioh Online" href="<?= BASE; ?>/micro-dados">Assistir Yugioh</a></li>
                    <li><a class="btn btn-green radius boxshadow" title="Loja Virtual" href="http://www.upinside.com.br">Loja do Acervo das Cartas</a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </header>

        <!--CONTEUDO-->
        <!--Bloco de Video-->
        <article class="container">
            <div class="content">
                <header>
                    <h1>Conheça o Acervo das Cartas</h1>
                    <p class="tagline">Contemple o site que faltava sobre<mark>Yugioh!</mark></p>
                </header>

                <video src="medias/Noragami_01_Dollars-OtakusFans-Anbient.mkv" width="400" controls></video>

                <aside>
                    <h1>Não Deixe de Visitar Nossa Loja Virtual <a title="Comprar Curso de HTML5 Agora!" target="_blank" href="http://upinside.com.br">Conhecer!</a></h1>
                </aside>

                <div class="clear"></div>
            </div>

            <footer class="bg-orange">
                <section class="content">
                    <h1>Veja Alguns Setores do Site Onde Você Pode Explorar!</h1>

                    <article>
                        <div class="thumb">
                            <img title="Sessão de Artigos Sobre Yugioh" alt="Artigos Yugioh" src="img/entendendo-o-html5.jpg"/>
                        </div>
                        <h1>Leia Artigos e Estratégias Sobre Yugioh!</h1>
                    </article> 

                    <article>
                        <div class="thumb">
                            <img title="Construa e Compartilhe Seu Próprio Deck de Yugioh!" alt="Criar Decks Yugioh" src="img/entendendo-o-html5.jpg"/>
                        </div>
                        <h1>Construa e Compartilhe Seu Próprio Deck! </h1>
                    </article> 

                    <article>
                        <div class="thumb">
                            <img title="Pesquisar Decks de Yugioh" alt="Pesquisar Decks de Yugioh" src="img/entendendo-o-html5.jpg"/>
                        </div>
                        <h1>Procure Por um Deck!</h1>
                    </article> 

                    <article>
                        <div class="thumb">
                            <img title="Assistir Yugioh Online" alt="Assistir Yugioh Online" src="img/entendendo-o-html5.jpg"/>
                        </div>
                        <h1>Assista Yugioh Online!</h1>
                    </article> 


                </section>
            </footer>
        </article>

        <!--Sessão Relacional-->

        <section class="container">
            <!--Container do Titulo-->
            <div class="content">

                <h1>Nesta sessão você irá encontrar todo tipo de conteúdo relacionado a Yugioh!:</h1>
                <p class="tagline">Conheça as estratégias e artigos úteis para você incrementar ao seu jogo, além da ecônomia das cartas notícias e muito mais!</p>

                <div class="clear"></div>
            </div>

            <!--Container dos Artigos-->
            <div class="container bg-blue">

                <div class="content">

                    <article>
                        <img src="img/tec_semantic.png" alt="HTML5 Semântico" title="Módulo Semântico"/>
                        <h1>Últimas Notícias</h1>
                        <p class="tagline">Notícias Sobre Yugioh!</p>
                    </article>

                    <article>
                        <img src="img/tec_drycss.png" alt="CSS produtivo com OOCSS" title="CSS produtivo OOCSS"/>
                        <h1>Artigos Úteis</h1>
                        <p class="tagline">Últimos Artigos Lançados no Site</p>
                    </article>

                    <article>
                        <img src="img/tec_forms.png" alt="Formulários" title="Módulo de Formulários"/>
                        <h1>Top Decks</h1>
                        <p class="tagline">Melhores Decks Profissionais e Amadores</p>
                    </article>

                    <article>
                        <img src="img/tec_midia.png" alt="Àudio e Vídeo na Web" title="Módulo de Áudio e Vídeo na Web"/>
                        <h1>Cartas em Alta</h1>
                        <p class="tagline">Cartas Que Estão em Alta</p>
                    </article>

                    <article>
                        <img src="img/tec_geo.png" alt="Geolocation e HTML5 Storage" title="Módulode Geolocation e HTML5 Storage"/>
                        <h1>Cartas em Queda</h1>
                        <p class="tagline">Cartas Que Estão em Baixa</p>
                    </article>

                    <article>
                        <img src="img/tec_microdados.png" alt="Distribuição com Micro Dados" title="Módulo de Distribuição com Micro Dados"/>
                        <h1>Cartas Mais Pesquisadas</h1>
                        <p class="tagline">Cartas Que Está Sendo Mais Procuradas em Nosso Acervo</p>
                    </article>

                    <div class="clear"></div>
                </div>
            </div>
        </section>

        <!--Seção Temática-->
        <section class="container">
            <div class="content">
                <div class="ses_title">
                    <h1>Top Decks</h1>
                    <p class="tagline">Saiba Tudo Sobre Os Melhores Decks Utilizados em Campeonatos!!</p>
                </div>

                <article><h1>Deck: <b>Olhos Vermelhos</b></h1></article>
                <article><h1>Tipo: <b>Dragão</b></h1></article>
                <article><h1>Elemento: <b>Dark</b></h1></article>
                <article><h1>Descrição: <b>Um deck baseado no clássico Dragão Negro de Olhos Vermelhos, voltado para um jogo focado em criaturas fortes e invocação especial do cemitério!</b></h1></article>



                <div class="clear"></div>
            </div>
        </section>

        <!--Retomada e Conversão-->
        <article class="container bg-blue">
            <div class="content">

                <header>
                    <h1>Faça Parte da Turma de HTML5, Matricule-se!</h1>
                    <p class="tagline">Começe Agora Mesmo. O Curso é <mark>100% em Vídeo Aulas</mark>, <mark>Online</mark> e <mark>On Demand!</mark></p>
                </header>

                <a title="Quero me Matricular no Curso de WS HTML5 Agora!" target="_blank" href="http://www.upinside.com.br">Comprar WS HTML5!</a>

                <footer>
                    <div>
                        Você estuda quando e onde quiser na melhor plataforma EAD. Com suporte diretamente com o tutor , e todo material disponível para download!
                    </div>
                </footer>

                <div class="clear"></div>
            </div>
        </article>

        <!--Content Visual-->
        <div class="container ">
            <div class="content">
                Acervo das Cartas. O lugar do duelista de Yugioh!
                <div class="clear"></div>
            </div>
        </div>

        <!--CONTEUDO-->

        <footer class="container bg-light">
            <section class="main_footer content ">
                <h1 class="fontzero">Sobre  a Acervo das Cartas</h1>

                <nav class="main_nav">
                    <h1 class="title">Mais sobre o Acervo das Cartas:</h1>
                    <ul>
                        <li><a title="Assista o Vídeo de Apresentação" href="#apresentacao">Assista o Vídeo</a></li>
                        <li><a title="Assista o Vídeo de Apresentação" href="#apresentacao">Assista o Vídeo</a></li>
                        <li><a title="Assista o Vídeo de Apresentação" href="#apresentacao">Assista o Vídeo</a></li>
                    </ul>
                </nav>

                <article class="main_social">   
                    <h1 class="title">Acervo das Cartas nas Redes Sociais:</h1>
                    <ul>
                        <li><a rel="nofollow" target="_blank" title="UpInside Treinamentos no Facebook" href="http://fb.com">Facebook</a></li>
                        <li><a rel="nofollow" target="_blank" title="UpInside Treinamentos no Google Plus" href="http://g.com">Google +</a></li>
                        <li><a rel="nofollow" target="_blank" title="UpInside Treinamentos no Twitter" href="http://tw.com">Twitter</a></li>
                    </ul>
                </article>

                <article class="main_copy">
                    <h1 class="fontzero">Fale Conosco</h1>
                    <p><b>Plataforma EAD:</b><a title="Plataforma EAD da UpInside" href="http://upinside.com.br">www.upinside.com.br</a></p>
                    <p><b>E-mail:</b>cursos@upinside.com.br</p>
                    <hr>
                    <p>&copy; <?= date('Y'); ?> - Copyrighted por Konami Digital Entertainment!</p>
                </article>

                <div class="clear"></div>
            </section>
        </footer>
    </body>
</html>
