<!DOCTYPE html>
<?php
define('BASE', 'http://127.0.0.1/PHP-upinside/HTML5/Modulos/02-html5-semantico/')
?>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        
        <!--[if it IE 9]>
        <script src="js/html5shiv.js"></script>
        <![endif]-->
        
        <title>Curso Work Series - HTML5 do Jeito Certo!</title>
    </head>
    <body>
        
        <header class="main_header">
            <h1 class="main_logo">
                <a title="Home" href="<?= BASE; ?>">
                    Curso Work Series - HTML5 do Jeito Certo!
                </a>
            </h1>
            
            <ul class="main_nav">
                <li><a title="" href=""></a></li>
            </ul>
        </header>
        
        <!--CONTEUDO-->
        
        <footer class="main_footer">
            <section>
                <h1>UpInside - Treinamentos</h1>
            </section>
        </footer>
        
        
        
    </body>
</html>
