<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

        <style>
            /*Resetar o estilo em todos os navegadores*/
            *{
                margin: 0;
                padding: 0;

                /*Faz a box se ajustar dependendo do tamanho da borda, assim o elemento não se quebra na pagina e fica responsivo em tudo relacionado a box.*/
                box-sizing: border-box; 
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;

                font-family: 'Open Sans', sans-serif;

            }

            /*       
                        Melhor estilo de utilizar CSS na prática por conta de economia de linhas.
                        .color_red{color: red;}
                       .color_blue{color: blue;}*/

            .content {
                /*Para não bugar os outros elementos da pagina*/
                float: left;
                width: 100%;
                padding:  5%;
                background: #ccc;

                background-image: url(http://eusouandroid.com/wp-content/uploads/Naruto-Sage-Mode-and-Sasuke-Mangekyou-Sharingan-Wallpaper.jpg);
                /*O Cover vai fazer a imagem se ajustar ao tamanho da box*/
                background-size: cover;
                /*Junta a imagen em todas a boxes como se fosse uma, além de ficar fixo quando faz a rolagem de pagina.*/
                background-attachment: fixed;
                /*Centralizou o conteudo obs: o conteudo não pode ter float e deve conter display:inline-box*/
                text-align: center;

            }

            .news{
                color: #000;
                padding:  30px;
                /*Define largura máxima, mas não uma minima tornando responsíva*/
                max-width: 500px;
                background: rgba(255,255,205, 0.7);
                display: inline-block;
                box-shadow: 0 2px 5px #555;




                /*                     border: 5px solid #fff;*/

                /*                     
                                     Preenche um campo foda da caixa 
                                     outline: 10px solid #DDD;*/
            }
            
            .news header{
                margin-bottom:  30px;
            }

            .title {
                font-size: 1.8em;
                font-weight: 600;
            }
            
            .tagline {
                margin-top: 15px;
                font-size: 1em;
                text-transform: uppercase;
            }
            
            .optin{
                display: block;
            }
            
            .optin input {
                display: block;
                padding: 15px;
                border: 1px solid #ccc;
                width: 100%;
                margin-bottom: 15px;
            }
            
            button{padding: 8px 25px;cursor: pointer;  font-size: 1.2em; font-weight: bold; color: #000; text-transform: uppercase;  border: 2px solid #22542d; background: #409d55;}
            button:hover{background: #44a85b;}
            
            /*Tirandoo float da box principal, e colocando outros elementos em float a pagina vai ficar toda errada necessitando de um clear.*/
            .clear {clear: both; }
        </style>

    </head>
    <body>
        <!--       
                Grau de importancia de especificidade
                Important = Colocar !important no CSS Ex: .artigo{ color: #fff !important; } PROCURAR NÃO USAR, SOBREPÕE TUDO.
                Inline = Escrever o código na própia tag NUNCA USAR
                ID = Fazer por ID no css [ # ] NUNCA USAR
                Class = Fazer por classes no css [ . ]
                Element = Fazer pelo elemento no css [ article ]
        -->

        <div class="content">
            <article class="news ">
                <header>
                    <h1 class="title">Inciando com CSS</h1>
                    <p class="tagline">Vamos aprender o básico e essencial em CSS! MIMIMI técnicas de css marketing digital coisas daora estilo bonitão estiloso twisted fate</p>
                </header>
                <form class="optin">
                        <input type="text" name="nome" placeholder="Insira seu Nome" required>
                        <input type="password" name="pass" placeholder="Insira sua Senha" required>   
                    <button> Enviar !</button>
                </form>

                <!--div criada para limpar os floats que foram dados fora da classe news-->
                <div class="clear"></div> 

            </article>

        </div>

    </body>
</html>
