<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title></title>

        <script src="js/modernizr.custom.64658.js"></script>
        <script>
            function teste(texto) {
                document.getElementById('teste').innerHTML += '<p>' + texto + '</p>';
            }

            window.onload = function () {

                if (Modernizr.localstorage) {
                    teste('Podemos armazenar offline!');
                }

                if (Modernizr.geolocation) {
                    teste('Podemos te encontrar!');
                }

                if (Modernizr.input.placeholder) {
                    teste('Podemos utilizar o placeholder!');
                }

                if (Modernizr.inputtypes.date) {
                    teste('Podemos utilizar type date');
                } else {
                    teste('<b> Não podemos utilizar type date</b>');
                    Modernizr.load('js/jquery.js');
                }
                ;
            };

            function check() {

                if (Modernizr.inputtypes.date) {
                    document.getElementById('date').setAttribute('type', 'date');
                } else {
                    $(function () {
                        var i = 0;
                        $('#date').attr('maxlenght', '10');
                        $('#date').keyup(function () {
                            i++;
                            if (i % 2 === 0 && i < 6) {
                                $(this).val($(this).val() + '/');
                            }
                        });
                    });

                }

            }

        </script>
    </head>
    <body>
        <div id="teste"><div>
                <input type="text" name="data" id="date" placeholder="Informe uma data" onfocus="check()"/>

                </body>
                </html>
