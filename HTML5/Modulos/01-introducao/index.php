<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Curso de HTML5 - UpInside Treinamentos</title>
        <meta name="description" content="Esta pagina web esta sendo construida no curso de HTML 5" />
        <meta name="viewport" content="width=device-width, initia-scale=1.0" />
        <link rel="shortcut icon" href="img/favicon.png" />
        <link rel="stylesheet" href="style.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">



    </head>
    <body>
        <p class="j_topo backtop">Topo</p>
        <!--Inicia Cabeçalho-->
        <header class="main_header container home">
            
            <div class="content">
                <img class="main_logo" title="Curso de HTML5" alt="[Curso de HTML5]" src="img/_logo.png" />
                <h1 class="fontzero">Curso de HTML5 - UpInside Treinamentos</h1>


                <ul class="main_menu">
                    <li><a href="#home" title="Curso de HTML5">Home</a></li>
                    <li><a href="#curso" title="Sobre o Curso de HTML5">O Curso</a></li>
                    <!--Target="_blank" para não sair do site / rel="nofollow" para não dar pontos no google para outro site-->
                    <li><a class="special" target="_blank" rel="nofollow" href="http://www.google.com/" title="Curso Work Series - HTML5 do Jeito Certo">Work Series</a></li> 
                    <li><a href="#videoaulas" title="Confira Vídeo Aulas do Curso de HTML5">Video Aulas</a></li>
                    <li><a href="#contato" title="Contato sobre o curso de HTML5">Fale Conosco</a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </header>
        <!--Fecha Cabeçalho-->

        <!--Inicia Conteudo-->
        <main class="container">
            <!--Inicia os Destaques do Site-->
            <div class="border_bottom border_bottom_destaque">
            <article class="destaque container">
                
                <div class="content">
                    <header>

                        <h1>Bem-vindo(a) ao Curso Gratuito de HTML5</h1>
                        <p class="tagline">Você esta pronto para aprender HTML5, do jeito certo de maneira descomplicada? </p>
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="recommend" data-size="large" data-show-faces="false" data-share="false"></div>
                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Compartilhar</a></div>

                    </header>
                    <div class="destaque_video">
                        <div class="ratio">
                            <iframe class="media" src="https://www.youtube.com/embed/OWFBqiUgspg" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="clear"></div>
                    </div>
                </div>

            </article>
            <!--Encerra o Destaque do Site-->
            <!--Apresentação dos Cursos-->
            <section class="container curso">
                <div class="content">
                    <header class="big_title">
                        <h1>Curso de HTML5</h1>
                        <p class="tagline">Confira o que você vai aprender no curso!</p>
                    </header>

                    <article class="curso_item">
                        <img title="Curso de HTML5" alt="[Curso de HTML5]" src="img/01.jpg" />
                        <h1>HTML5  !</h1>
                        <p>Aprenda a utilizar o HTML5 do jeito certo! Segmentando seu site e distribuindo o conteúdo de forma otimizada!</p>
                        <span><time datetime="<?= date('Y-m-d'); ?>"><?= date('d/m/Y \à\s H:i\h\s'); ?></time> em <b>Matérias</b>  </span>   
                    </article>

                    <article class="curso_item">
                        <img title="Curso de CSS" alt="[Curso de CSS]" src="img/02.jpg" />
                        <h1>CSS  !</h1>
                        <p>Aprenda a utilizar o CSS do jeito certo! Deixando o seu site super animal com as ultimas técnicas de CSS!</p>
                        <span><time datetime="<?= date('Y-m-d'); ?>"><?= date('d/m/Y \à\s H:i\h\s'); ?></time> em <b>Matérias</b>  </span>                     
                    </article>

                    <article class="curso_item">
                        <img title="Curso de jQuery" alt="[Curso de jQuery]" src="img/03.jpg" />
                        <h1>jQuery  !</h1>
                        <p>Aprenda a utilizar o jQuery do jeito certo! Deixando o seu site super animal com as incriveis interações que o jQuery pode lhe proporcionar!</p>
                        <span><time datetime="<?= date('Y-m-d'); ?>"><?= date('d/m/Y \à\s H:i\h\s'); ?></time> em <b>Matérias</b>  </span>                     
                    </article>

                    <article class="curso_item">
                        <img title="Curso de WorkSeries" alt="[Curso de WorkSeries]" src="img/04.jpg" />
                        <h1>WorkSeries  !</h1>
                        <p>Aprenda como colocar tudo o que você aprendeu em pratica com altas técnicas de venda de site</p>
                        <span><time datetime="<?= date('Y-m-d'); ?>"><?= date('d/m/Y \à\s H:i\h\s'); ?></time> em <b>Matérias</b>  </span>                     
                    </article>
                    <div class="clear"></div>
                </div>

                <footer class="container curso_footer">
                    <div class="content">
                        <img title="Confira o curso completo HTML5 do Jeito Certo" alt="[Confira o que você vai aprender no curso!]" src="img/05.png"/>
                        <h1>Curso de HTML5 do Jeito Certo!</h1>
                        <p> Aprenda a Utilizar Todo o Poder do HTML5 Semântico Somado a Avançadas Técnicas. </p>
                        <a class="btn" target="_blank" href="http://www.google.com/" title="Conheça o curso HTML5 do Jeito Certo!">Conhecer Agora!</a>
                        <div class="clear"></div>
                    </div>
                </footer>


            </section>
            <!--Encerra a apresentação dos Cursos-->

            <!--Inicia as Video Aulas-->
            <section class="container videoaulas">
                <div class="content">
                    <header class="big_title">
                        <h1>Video Aulas do Curso</h1>
                        <p class="tagline">Confira as video aulas do curso de HTML5!<p>
                    </header>

                    <div class="videoaulas_videos">
                        <article>
                            <img title="Curso de HTML5" alt="[Curso de HTML5]" src="img/01.jpg" />
                            <h1>HTML5  !</h1>
                            <p>Aprenda a utilizar o HTML5 do jeito certo!</p>                      
                        </article>

                        <article>
                            <img title="Curso de CSS" alt="[Curso de CSS]" src="img/02.jpg" />
                            <h1>CSS  !</h1>
                            <p>Aprenda a utilizar o CSS do jeito certo!</p>                   
                        </article>

                        <article>
                            <img title="Curso de jQuery" alt="[Curso de jQuery]" src="img/03.jpg" />
                            <h1>jQuery  !</h1>
                            <p>Aprenda a utilizar o jQuery do jeito certo!</p>                     
                        </article>

                        <article>
                            <img title="Curso de HTML5" alt="[Curso de HTML5]" src="img/01.jpg" />
                            <h1>HTML5  !</h1>
                            <p>Aprenda a utilizar o HTML5 do jeito certo!</p>                      
                        </article>

                        <article>
                            <img title="Curso de CSS" alt="[Curso de CSS]" src="img/02.jpg" />
                            <h1>CSS  !</h1>
                            <p>Aprenda a utilizar o CSS do jeito certo!</p>                   
                        </article>

                        <article>
                            <img title="Curso de jQuery" alt="[Curso de jQuery]" src="img/03.jpg" />
                            <h1>jQuery  !</h1>
                            <p>Aprenda a utilizar o jQuery do jeito certo!</p>                     
                        </article>
                    </div>

                    <aside class="videoaulas_siderbar">
                        <div class="border_bottom">
                             <div class="content">

                                <header>
                                    <h1 class="title">Comentários:</h1>
                                </header>

                                <article class="videoaulas_siderbar_comment">
                                    <img title="Comentario de Raquel V. Assumpção" alt="[Comentario de Raquel V. Assumpção]" src="img/linda.jpg" />
                                    <h1><span class="fontzero">Comentário de </span> Raquel V. Assumpção</h1>
                                    <p>Esse é show de bola quero nem saber comprei mesmo to ficando inteligentão, erooo!</p>
                                </article>

                                <article class="videoaulas_siderbar_comment">
                                    <img title="Comentario de Ahri" alt="[Comentario de Ahri]" src="img/Arcade_Ahri_1920x1200.jpg" />
                                    <h1><span class="fontzero">Comentário de </span> Ahri</h1>
                                    <p>Ahahaha... Pulsação parça pulsação...</p>
                                </article>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </aside>
                    <div class="clear"></div>
                </div>
            </section>
            <!--Encerra as Video Aulas-->

            <!--Inicia o Contato-->
            <article class="container contato">
                <div class="content">
                    <header>
                        <h1>Entre em Contato</h1>
                        <p class="tagline">Este formulário está aqui apenas para estudos do curso de HTML5</p>
                    </header>

                    <form class="contato_form j_formsubmit" action="" method="post">
                        <label class="medium">
                            <span>Nome:</span>
                            <input type="text" name="nome" title="Informe Seu Nome!" placeholder="Informa Seu Nome" required />
                        </label>

                        <label class="medium">
                            <span >E-Mail:</span>
                            <input type="email" name="email" title="Informe Seu Email!" placeholder="Informa Seu E-Mail" required />
                        </label>

                        <label>
                            <span>Mensagem:</span>
                            <textarea rows="3" name="mensagem" title="Deixe sua Mensagem!" placeholder="Deixe  sua Mensagem:" required></textarea>
                        </label>

                        <img class="form_load" title="Aguarde, Enviando Contato!" alt="[Aguarde, Enviando Contato!]" src="img/ajax-loader.gif" />
                        
                        <button class="btn">Enviar Mensagem!</button>
                    </form>
                    <div class="clear"></div>
                </div>
            </article>
            <!--Encerra o Contato-->

        </main>
        <!--Fecha Conteudo-->
        <footer class="container main_footer">
            <div class="border_bottom">
            <div class="ftcontent">
                <h1 class="title">Curso de HTML5</h1>
                <nav>
                    <h1 class="subtitle">UpInside Treinamentos</h1>
                    <ul class="main_footer_social">
                        <li><a href="//www.upinside.com.br"  target="_blank" title="">UpInside</a></li>
                        <li>I</li>
                        <li><a href="//www.youtube.com.br"  target="_blank" title="">YouTube</a></li>
                        <li>I</li>
                        <li><a href="//www.facebook.com.br"  target="_blank" title="">Facebook</a></li>
                    </ul>
                </nav>
                <div class="clear"></div>
            </div>
            </div>
        </footer>
        <!--Fecha Rodapé-->
        <script src="js/jquery.js"></script>
        <script src="js/script.js"></script>
        
        <div id="fb-root"></div>        
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    </body>
</html>
